//
//  ClickButton.swift
//  AnimationUIButton
//
//  Created by Руслан on 27.08.2019.
//  Copyright © 2019 Ruslan. All rights reserved.
//

import UIKit

class ClickButton: UIButton {
    
    private let shapeLayer = CAShapeLayer()
    private let shapeLayerSecond = CAShapeLayer()
    public var heigtView: CGFloat = 0.0
    public var weightView: CGFloat = 0.0
    private var defaultWeight: CGFloat = 0.0
    private var defaultHeight: CGFloat = 0.0
    private var defaultOriginX: CGFloat = 0.0
    private var defaultOriginY: CGFloat = 0.0
    
    private let colorStandart: UIColor = {
        let standartColor = UIColor(red: 31 / 255, green: 130 / 255, blue: 118 / 255, alpha: 1.0)
        return standartColor
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        installButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        installButton()
    }
    
    private func installButton() {
        backgroundColor = .white
        setTitle("Click Me", for: .normal)
        titleLabel?.font = UIFont(name: "Futura Medium", size: 15)
        layer.cornerRadius = 30
        layer.borderWidth = 3.0
        layer.borderColor = colorStandart.cgColor
        startAnimation()
    }
    
    private func didSelectClickButton() {
        defaultWeight = frame.size.width
        defaultHeight = frame.size.height
        defaultOriginX = frame.origin.x
        defaultOriginY = frame.origin.y
        addTarget(self, action: #selector(selectClickButton), for: .touchUpInside)
    }
    
    @objc func selectClickButton() {
        UIView.animate(withDuration: 0.1, animations: { [weak self] in
            guard let self = self else { return }
            self.updateClickButton()
        }) { (finished) in
            UIView.animate(withDuration: 2.0, delay: 0.5, options: .curveEaseOut, animations: { [weak self] in
                guard let self = self else { return }
                self.installShapeLayer()
            }) { [weak self] (finished) in
                guard let self = self else { return }
                UIView.animate(withDuration: 0.5, animations: { [weak self] in
                   guard let self = self else { return }
                   self.installShapeLayerSecond()
                }, completion: { [weak self] (finshed) in
                    guard let self = self else { return }
                    self.setupBasicAnimation()
                })
            }
        }
    }
    
    private func updateClickButton() {
        self.isEnabled = false
        self.frame.size.height = 150
        self.frame.size.width = 150
        self.frame.origin.x = weightView / 2 - self.frame.size.width / 2
        self.frame.origin.y = heigtView / 2 - self.frame.size.height / 2
        backgroundColor = .white
        layer.cornerRadius = self.frame.size.width / 2
        layer.borderColor = UIColor.clear.cgColor
        clipsToBounds = true
    }
    
    private func installShapeLayer() {
         installSettingsShapeLayer(layerShape: shapeLayer, strokeEnd: 1.0, colorStoke: UIColor.lightGray.cgColor)
        
        layer.addSublayer(shapeLayer)
    }
    
    private func installShapeLayerSecond() {
        installSettingsShapeLayer(layerShape: shapeLayerSecond, strokeEnd: 0.0, colorStoke: colorStandart.cgColor)
        
        shapeLayer.addSublayer(shapeLayerSecond)
    }
    
    private func installSettingsShapeLayer(layerShape: CAShapeLayer, strokeEnd: CGFloat, colorStoke: CGColor) {
        let bounds = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y, width: self.frame.size.width, height: self.frame.size.height)
        layerShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: self.frame.size.width / 2, height: self.frame.size.height / 2)).cgPath

        layerShape.fillColor = UIColor.clear.cgColor
        layerShape.strokeColor = colorStoke
        layerShape.lineWidth = 15.0
        layerShape.strokeEnd = strokeEnd
    }
    
    private func startAnimation() {
        UIView.animate(withDuration: 0.5, delay: 1.0, options: .curveEaseOut, animations: { [weak self] in
            guard let self = self else { return }
            self.setTitleColor(self.colorStandart, for: .normal)
            self.backgroundColor = self.colorStandart
        }) { [weak self] (finished)  in
            guard let self = self else { return }
             self.setTitleColor(.white, for: .normal)
             self.didSelectClickButton()
        }
    }
    
    private func setupBasicAnimation() {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.delegate = self
        animation.duration = 2.0
        animation.fromValue = 0
        animation.toValue = 1
        
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        shapeLayerSecond.strokeEnd = 1.0
        
        shapeLayerSecond.add(animation, forKey: "animateCircle")
    }
    
}

extension ClickButton: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        UIView.animate(withDuration: 0.5) { [weak self] in
            guard let self = self else { return }
            self.returnDefaultButton()
        }
    }
    
    private func returnDefaultButton() {
        layer.sublayers = nil
        setTitle("", for: .normal)
        frame.size.height = defaultHeight
        frame.size.width = defaultWeight
        frame.origin.x = defaultOriginX
        frame.origin.y = defaultOriginY
        layer.cornerRadius = 30
        layer.borderWidth = 3.0
        backgroundColor = colorStandart
        setImage(UIImage(named: "icon"), for: .normal)
    }
}


