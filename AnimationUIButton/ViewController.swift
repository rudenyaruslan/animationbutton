//
//  ViewController.swift
//  AnimationUIButton
//
//  Created by Руслан on 27.08.2019.
//  Copyright © 2019 Ruslan. All rights reserved.
//

import UIKit

protocol GetSizeUIView: class{
    func getDataView(weight: Float, height: Float)
}

class ViewController: UIViewController {
    
    var clickButton = ClickButton()
    weak var delegate: GetSizeUIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        installConstarints()
        clickButton.heigtView = self.view.frame.height
        clickButton.weightView = self.view.frame.width
    }


    private func installConstarints() {
        view.addSubview(clickButton)
        clickButton.translatesAutoresizingMaskIntoConstraints = false
        clickButton.heightAnchor.constraint(equalToConstant: 65.0).isActive = true
        clickButton.widthAnchor.constraint(equalToConstant: 250.0).isActive = true
        clickButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        clickButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}

